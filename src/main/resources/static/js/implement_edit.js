var prefix = ctx + "implement";
$(document).ready(function () {
    var caseId = $("#caseId").val();
    //加载案件分类信息
    initCaseTypeData(caseId);
    //加载执行申请人数据
    initApplicationUserData();
    //加载被执行人数据
    initProposerUserInfo(caseId);
    //加载证据信息
    initEvidenceInfo(caseId);
});

/**
 * 查询案件的证据信息
 *
 * @param caseId
 * @author Qiu Rui
 * @date 2020/11/11 6:39 下午
 * @return
 */
function initEvidenceInfo(caseId) {
    $.ajax({
        url: prefix + "/initEvidenceInfo/"+caseId,
        type: "get",
        dataType: "json",
        contentType: 'application/json',
        success: function (result) {
            if (result.code == web_status.SUCCESS) {
                //填充表格数据
                var data=result.data;
                $("#bidding_amount").val(data.biddingAmount);
                $("#arbitration_commission_name").val(data.arbitrationCommissionName);
                var evis=data.attachmentEvidenceVos;
                //拼接数据
                var evisLength=evis.length;
                for (var i = 0; i < evisLength; i++) {
                    getEviData(evis[i],evisLength-i);
                    /*$("#bootstrap-table-evi").bootstrapTable('insertRow', {
                        index: 0, // 你想插入到哪，0表示第一行
                        row: getEviData(evis[i])
                    })*/
                }
            }
        }
    });
}

function getEviData(data,kk) {
    var attachmentUrl=data.fileUrl;
    var attachmentName=data.fileName;
    var attachmentSize=data.fileSize;
    //当前表格的数据总数
    var rows=$("#bootstrap-table-evi").bootstrapTable("getData").length;
    var num=rows+1;
    var temp = { //这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
        //"num":"0_"+num,
        "fileSize": attachmentSize,
        "fileName": attachmentName
    };
    //隐藏域添加值，最后提交的时候用
    var value="id:"+data.id+",fileSize:"+attachmentSize+",fileName:\""+
        attachmentName+"\",fileUrl:\""+attachmentUrl+"\"";
    $("#envice_hidden").append("<input type='hidden' name='"+attachmentName+"' value='"+value+"'/>");
    return temp;
}

/**
 * 加载被执行人数据
 *
 * @param caseId
 * @author Qiu Rui
 * @date 2020/11/11 5:49 下午
 * @return
 */
function initProposerUserInfo(caseId) {
    $.ajax({
        url: prefix + "/initProposerUserInfo/"+caseId,
        type: "get",
        dataType: "json",
        contentType: 'application/json',
        success: function (result) {
            if (result.code == web_status.SUCCESS) {
                //填充表格数据
                var data=result.data;
                var dataLength=data.length;
                for (var i = 0; i < dataLength; i++) {
                    getBoBoData(data[i],dataLength-i);
                    /*$("#bootstrap-table-bo").bootstrapTable('insertRow', {
                        index: 0, // 你想插入到哪，0表示第一行
                        row: getBoBoData(data[i])
                    })*/
                }
            }
        }
    });
}

function getBoBoData(data,kk) {
    var partyType=data.proposerType;
    var partyTypeDes=data.proposerTypeDes;
    var proposer_name=data.proposerName;
    var proposer_id_card_no=data.proposerIdCardNo;
    var proposer_work_unit=data.proposerWorkUnit;
    var proposer_mobile=data.proposerMobile;
    var proposer_email=data.proposerEmail;
    var proposer_country=data.proposerCountry;
    var proposer_nation=data.proposerNation;
    var proposer_address=data.proposerAddress;
    var proposer_gender=data.proposerGender;
    var proposer_legal_person_name=data.proposerLegalPersonName;
    var proposer_legal_person_card=data.proposerLegalPersonCard;
    var proposer_job=data.proposerJob;
    var sexDes=data.proposerGenderDes;
    var id=data.id;
    var rows=$("#bootstrap-table-bo").bootstrapTable("getData").length;
    var num=rows+1;
    var temp = { //这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
        //"num":"0_"+num,
        "proposerTypeDes": partyTypeDes,
        "proposerName": proposer_name,
        "proposerIdCardNo": proposer_id_card_no,
        "proposerWorkUnit": proposer_work_unit,
        "proposerMobile": proposer_mobile,
        "proposerEmail": proposer_email,
        "proposerCountry": proposer_country,
        "proposerNation": proposer_nation,
        "proposerAddress": proposer_address,
        "proposerGenderDes": sexDes,
        "proposerLegalPersonName": proposer_legal_person_name,
        "proposerLegalPersonCard": proposer_legal_person_card,
        "proposerJob": proposer_job
    };
    //隐藏域添加值，最后提交的时候用
    var value="id:"+id+",proposerType:"+partyType+",proposerName:\""+proposer_name+"\",proposerIdCardNo:\""+proposer_id_card_no+"\",proposerWorkUnit:\""+proposer_work_unit +"\",proposerMobile:\""+proposer_mobile
        +"\",proposerEmail:\""+ proposer_email+"\",proposerCountry:\""+proposer_country+"\",proposerNation:\""+proposer_nation+"\",proposerAddress:\""+proposer_address
        +"\",proposerGender:"+proposer_gender+",proposerLegalPersonName:\""+proposer_legal_person_name+"\",proposerLegalPersonCard:\""+proposer_legal_person_card +"\",proposerJob:\""+proposer_job+"\"";
    $("#bo_hidden").append("<input type='hidden' name='"+proposer_name+"' value='"+value+"'/>");
    return temp;
}

/**
 * 修改页面加载案件分类信息
 *
 * @param caseId
 * @author Qiu Rui
 * @date 2020/11/11 4:44 下午
 * @return
 */
function initCaseTypeData(caseId) {
    $.ajax({
        url: prefix + "/initCaseTypeData/"+caseId,
        type: "get",
        dataType: "json",
        contentType: 'application/json',
        success: function (result) {
            if (result.code == web_status.SUCCESS) {

                $("#caseTypeVo_applicant_enterprise_industry").val(result.data.applicantEnterpriseIndustry);
                $("#caseTypeVo_case_type").val(result.data.caseType);
                $("#caseTypeVo_enforcement_basis").val(result.data.enforcementBasis);
                $("#caseTypeVo_loan_get_way").val(result.data.loanGetWay);

                $("#caseTypeVo_exec").distpicker({
                    province: result.data.execProvince,
                    city: result.data.execCity
                });

                //证据信息中的标的和仲裁委名称
                $("#bidding_amount").val(result.data.biddingAmount);
                $("#arbitration_commission_name").val(result.data.arbitrationCommissionName);
            }
        }
    });
}
