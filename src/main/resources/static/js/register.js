var /**/prefix = ctx;
var icon = "<i class='fa fa-times-circle'></i> ";
//手机号码验证
jQuery.validator.addMethod("isPhone", function(value, element) {
    var length = value.length;
    var mobile = /^(13[0-9]{9})|(18[0-9]{9})|(14[0-9]{9})|(17[0-9]{9})|(15[0-9]{9})$/;
    return this.optional(element) || (length == 11 && mobile.test(value));
}, "手机号码格式不正确");
/**************************************************************************
 身份号码排列顺序从左至右依次为：六位数字地址码，八位数字出生日期码，三位数字顺序码和一位数字校验码。
 地址码表示编码对象常住户口所在县(市、旗、区)的行政区划代码。
 出生日期码表示编码对象出生的年、月、日，其中年份用四位数字表示，年、月、日之间不用分隔符。
 顺序码表示同一地址码所标识的区域范围内，对同年、月、日出生的人员编定的顺序号。
 顺序码的奇数分给男性，偶数分给女性。
 校验码是根据前面十七位数字码，按照ISO 7064:1983.MOD 11-2校验码计算出来的检验码。
 15位校验规则 6位地址编码+6位出生日期+3位顺序号
 18位校验规则 6位地址编码+8位出生日期+3位顺序号+1位校验位
 校验位规则     公式:∑(ai×Wi)(mod 11)……………………………………(1)
 公式(1)中：
 i----表示号码字符从右至左包括校验码在内的位置序号；
 ai----表示第i位置上的号码字符值；
 Wi----示第i位置上的加权因子，其数值依据公式Wi=2^(n-1）(mod 11)计算得出。
 i 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1
 Wi 7 9 10 5 8 4 2 1 6 3 7 9 10 5 8 4 2 1
 ****************************************************************************/
//身份证号验证
jQuery.validator.addMethod("isIdCard", function(value, element) {
    //身份证号码为15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X
    var idCard = /(^\d{15}$)|(^\d{17}(\d|X)$)/;
    return this.optional(element) || (idCard.test(value));
}, "身份证号码格式不正确");
//手机号唯一性校验
jQuery.validator.addMethod("isUniquePhone", function(value, element) {
    return checkUniquePhone(value);
}, "手机号已经注册");
//注册协议勾选校验
jQuery.validator.addMethod("isRead", function(value, element) {
    if (null == value || '' == value || undefined == value) {
        return false;
    }
    return true;
}, "注册是必须勾选同意注册协议");

/**
 * 自然人注册页面数据初始化
 *
 * @param null
 * @author Qiu Rui
 * @date 2020/11/6 6:22 下午
 * @return
 */
function initPersonalData() {
    //自然人注册信息提交
    $("#personal_submit").on("click",function () {
        if ($('#personal_signupForm').validate({
            rules: {
                realName: {
                    required: true,
                    minlength: 2,
                    maxlength:10
                },
                idCard: {
                    required: true,
                    maxlength: 18,
                    isIdCard:true
                },
                address: {
                    required: true,
                    maxlength: 300
                },
                phone: {
                    required: true,
                    isPhone: true,
                    isUniquePhone:true
                },
                phoneCode: {
                    required: true,
                    maxlength: 6
                },
                password: {
                    required: true,
                    minlength:6,
                    maxlength: 20
                },
                confirmPassword: {
                    required: true,
                    minlength:6,
                    maxlength: 20,
                    equalTo: "#personal_password"
                },
                idCardImgTopFileUrl:{
                    required: true
                },
                idCardImgBottomFileUrl:{
                    required: true
                },
                personalAcceptTerm: {
                    isRead:true
                }
            },
            messages: {
                realName: {
                    required: icon + "请输入您的姓名",
                    minlength: icon + "姓名必须在2个字以上",
                    maxlength: icon + "姓名必须在10个字以下"
                },
                idCard: {
                    required: icon + "请输入您的身份证号",
                    maxlength: icon + "身份证号必须在18位以下",
                    isIdCard: icon + "身份证号码格式不正确"
                },
                address: {
                    required: icon + "请输入您的地址",
                    maxlength: icon + "地址不能超过300个字",
                },
                phone: {
                    required: icon + "请输入您的手机号码",
                    isPhone: icon + "手机号码格式不正确",
                    isUniquePhone: icon + "手机号已经被注册"
                },
                phoneCode: {
                    required: icon + "请输入您的手机验证码",
                    maxlength: icon + "手机验证码不能超过6位"
                },
                password: {
                    required: icon + "请输入您的密码",
                    minlength: icon + "密码必须6个字符以上",
                    maxlength: icon + "密码必须20个字符以下"
                },
                confirmPassword: {
                    required: icon + "请再次输入密码",
                    minlength: icon + "密码必须6个字符以上",
                    maxlength: icon + "密码必须20个字符以下",
                    equalTo: icon + "两次输入的密码不一致"
                },
                idCardImgTopFileUrl:{
                    required: icon + "请上传身份证正面图片"
                },
                idCardImgBottomFileUrl:{
                    required: icon + "请上传身份证背面图片"
                },
                personalAcceptTerm: {
                    isRead:icon + "必须勾选同意注册协议"
                }
            }
        }).form()){
            $.ajax({
                url:"/registerAction",
                type: "post",
                dataType: "json",
                data: $('#personal_signupForm').serialize(),
                success: function (result) {
                    if (result.code == web_status.SUCCESS) {
                        layer.msg("注册成功", {
                            icon: 1,
                            time: 1500,
                            shade: [0.1, '#8F8F8F']
                        },function() {
                            window.location.href = "/login";
                        });
                    } else {
                        $.modal.alertWarning(result.msg);
                    }
                }
            });
            $.ajax(config)
        }
    });
    $("#fileinput-only-personal-bottom").fileinput({
        //详细参数配置请参考  http://doc.ruoyi.vip/ruoyi/document/zjwd.html#jasny-bootstrap
        'theme': 'explorer-fas',
        'uploadUrl': '/oss/uploadFile',
        overwriteInitial: false,
        initialPreviewAsData: true,
        maxFileCount: 1, //表示允许同时上传的最大文件个数
        enctype: 'multipart/form-data',
        validateInitialCount:true,
        allowedFileExtensions:["jpg", "png", "jpeg", "pdf"],
        msgFilesTooMany: "选择上传的文件数量({n}) 超过允许的最大数值{m}！",
        autoReplace: false, //是否自动替换当前图
        uploadAsync: true, //是否为异步上传
        //默认显示的图片预览
        initialPreview: [
            //"/img/profile.jpg"
        ]
    });
    //上传成功之后的处理
    $("#fileinput-only-personal-bottom").on("fileuploaded", function(event, data, proviewId, index) {
        var fileUrl = data.response.fileUrl;
        var fileSize = data.response.fileSize;
        var fileName = data.response.fileName;
        $('input[name="idCardImgBottomFileUrl"]').val(fileUrl);
        $('input[name="idCardImgBottomFileName"]').val(fileName);
        $('input[name="idCardImgBottomFileSize"]').val(fileSize);
    });
    //清空预览图片的响应事件
    $("#fileinput-only-personal-bottom").on("filecleared",function(event, data, msg){
        //清除隐藏域的值
        $("#idCardImgTopFileUrl").val("");
        $("#idCardImgTopFileName").val("");
        $("#idCardImgTopFileSize").val("");
    });

    $("#fileinput-only-personal-top").fileinput({
        //详细参数配置请参考  http://doc.ruoyi.vip/ruoyi/document/zjwd.html#jasny-bootstrap
        'theme': 'explorer-fas',
        'uploadUrl': '/oss/uploadFile',
        overwriteInitial: false,
        initialPreviewAsData: true,
        maxFileCount: 1, //表示允许同时上传的最大文件个数
        enctype: 'multipart/form-data',
        validateInitialCount:true,
        allowedFileExtensions:["jpg", "png", "jpeg", "pdf"],
        msgFilesTooMany: "选择上传的文件数量({n}) 超过允许的最大数值{m}！",
        autoReplace: false, //是否自动替换当前图
        uploadAsync: true, //是否为异步上传
        //默认显示的图片预览
        initialPreview: [
            //"/img/profile.jpg"
        ]
    });
    //上传成功之后的处理
    $("#fileinput-only-personal-top").on("fileuploaded", function(event, data, proviewId, index) {
        var fileUrl = data.response.fileUrl;
        var fileSize = data.response.fileSize;
        var fileName = data.response.fileName;
        $('input[name="idCardImgTopFileUrl"]').val(fileUrl);
        $('input[name="idCardImgTopFileName"]').val(fileName);
        $('input[name="idCardImgTopFileSize"]').val(fileSize);
    });
    //清空预览图片的响应事件
    $("#fileinput-only-personal-top").on("filecleared",function(event, data, msg){
        //清除隐藏域的值
        $("#idCardImgTopFileUrl").val("");
        $("#idCardImgTopFileName").val("");
        $("#idCardImgTopFileSize").val("");
    });
}

/**
 * 校验手机号唯一性
 *
 * @param null
 * @author Qiu Rui
 * @date 2020/11/6 5:02 下午
 * @return
 */
function checkUniquePhone(phone) {
    //请求后台校验逻辑
    return true;
}

//=======================================企业注册相关=========================================
//==========================================================================================
/**
 * 企业注册页面数据初始化
 *
 * @param null
 * @author Qiu Rui
 * @date 2020/11/6 6:23 下午
 * @return
 */
function initCompanyData() {
    $("#company_submit").on("click",function () {
        if ($('#company_signupForm').validate({
            rules: {
                realName: {
                    required: true
                },
                organizationCode: {
                    required: true
                },
                enterpriseIndustry: {
                    required: true
                },
                legalPersonName: {
                    required: true
                },
                legalPersonCard: {
                    required: true,
                    maxlength: 18,
                    isIdCard:true
                },
                idCard: {
                    required: true,
                    minlength:6,
                    maxlength: 20
                },
                contacts:{
                    required: true
                },
                companyAddress: {
                    required: true,
                    maxlength: 300
                },
                phone: {
                    required: true,
                    isPhone: true,
                    isUniquePhone:true
                },
                phoneCode: {
                    required: true,
                    maxlength: 6
                },
                password: {
                    required: true,
                    minlength:6,
                    maxlength: 20
                },
                confirmPassword: {
                    required: true,
                    minlength:6,
                    maxlength: 20,
                    equalTo: "#company_password"
                },
                idCardImgTop:{
                    required: true
                },
                idCardImgBottom:{
                    required: true
                },
                businessImg:{
                    required: true
                },
                companyAcceptTerm: {
                    isRead:true
                }
            },
            messages: {
                realName: {
                    required: icon + "请输入企业名称"
                },
                organizationCode: {
                    required: icon + "请输入组织机构代码"
                },
                enterpriseIndustry: {
                    required: icon + "请选择企业类型"
                },
                legalPersonName: {
                    required: icon + "请输入法人名称"
                },
                legalPersonCard: {
                    required: icon + "请输入您的身份证号",
                    maxlength: icon + "身份证号必须在18位以下",
                    isIdCard: icon + "身份证号码格式不正确"
                },
                idCard: {
                    required: icon + "请输入组织机构代码",
                    maxlength: icon + "请输入正确组织机构代码"
                },
                contacts:{
                    required: icon + "请输入联系人"
                },
                companyAddress: {
                    required: icon + "请输入您的地址",
                    maxlength: icon + "地址不能超过300个字",
                },
                phone: {
                    required: icon + "请输入您的手机号码",
                    isPhone: icon + "手机号码格式不正确",
                    isUniquePhone: icon + "手机号已经被注册"
                },
                phoneCode: {
                    required: icon + "请输入您的手机验证码",
                    maxlength: icon + "手机验证码不能超过6位"
                },
                password: {
                    required: icon + "请输入您的密码",
                    minlength: icon + "密码必须6个字符以上",
                    maxlength: icon + "密码必须20个字符以下"
                },
                confirmPassword: {
                    required: icon + "请再次输入密码",
                    minlength: icon + "密码必须6个字符以上",
                    maxlength: icon + "密码必须20个字符以下",
                    equalTo: icon + "两次输入的密码不一致"
                },
                idCardImgTop:{
                    required: icon + "请上传身份证正面图片"
                },
                idCardImgBottom:{
                    required: icon + "请上传身份证背面图片"
                },
                businessImg:{
                    required: icon + "请上传营业执照副本图片"
                },
                companyAcceptTerm: {
                    isRead:icon + "必须勾选同意注册协议"
                }
            }
        }).form()){
            $.ajax({
                url:"/registerAction",
                type: "post",
                dataType: "json",
                data: $('#company_signupForm').serialize(),
                success: function (result) {
                    if (result.code == web_status.SUCCESS) {
                        layer.msg("注册成功", {
                            icon: 1,
                            time: 1500,
                            shade: [0.1, '#8F8F8F']
                        },function() {
                            window.location.href = "/login";
                        });
                    } else {
                        $.modal.alertWarning(result.msg);
                    }
                }
            });
            $.ajax(config)
        }
    });
    //上传法人身份证正面照片信息
    $("#fileinput-only-company-legal-top").fileinput({
        //详细参数配置请参考  http://doc.ruoyi.vip/ruoyi/document/zjwd.html#jasny-bootstrap
        'theme': 'explorer-fas',
        'uploadUrl': '/oss/uploadFile',
        overwriteInitial: false,
        initialPreviewAsData: true,
        maxFileCount: 1, //表示允许同时上传的最大文件个数
        enctype: 'multipart/form-data',
        validateInitialCount:true,
        allowedFileExtensions:["jpg", "png", "jpeg", "pdf"],
        msgFilesTooMany: "选择上传的文件数量({n}) 超过允许的最大数值{m}！",
        autoReplace: false, //是否自动替换当前图
        uploadAsync: true, //是否为异步上传
        //默认显示的图片预览
        initialPreview: [
            //"/img/profile.jpg"
        ]
    });
    //上传成功之后的处理
    $("#fileinput-only-company-legal-top").on("fileuploaded", function(event, data, proviewId, index) {
        var fileUrl = data.response.fileUrl;
        var fileSize = data.response.fileSize;
        var fileName = data.response.fileName;
        $('input[name="legalPersonCardTopFileUrl"]').val(fileUrl);
        $('input[name="legalPersonCardTopFileName"]').val(fileName);
        $('input[name="legalPersonCardTopFileSize"]').val(fileSize);

    });
    //清空预览图片的响应事件        r
    $("#fileinput-only-company-legal-top").on("filecleared",function(event, data, msg){
        $('input[name="legalPersonCardTopFileUrl"]').val("");
        $('input[name="legalPersonCardTopFileName"]').val("");
        $('input[name="legalPersonCardTopFileSize"]').val("");
    });

    //上传律师执业证书-照片信息
    $("#fileinput-only-lay-certificate").fileinput({
        //详细参数配置请参考  http://doc.ruoyi.vip/ruoyi/document/zjwd.html#jasny-bootstrap
        'theme': 'explorer-fas',
        'uploadUrl': '/oss/uploadFile',
        overwriteInitial: false,
        initialPreviewAsData: true,
        maxFileCount: 1, //表示允许同时上传的最大文件个数
        enctype: 'multipart/form-data',
        validateInitialCount:true,
        allowedFileExtensions:["jpg", "png", "jpeg", "pdf"],
        msgFilesTooMany: "选择上传的文件数量({n}) 超过允许的最大数值{m}！",
        autoReplace: false, //是否自动替换当前图
        uploadAsync: true, //是否为异步上传
        //默认显示的图片预览
        initialPreview: [
            //"/img/profile.jpg"
        ]
    });
    //上传成功之后的处理
    $("#fileinput-only-lay-certificate").on("fileuploaded", function(event, data, proviewId, index) {
        var fileUrl = data.response.fileUrl;
        var fileSize = data.response.fileSize;
        var fileName = data.response.fileName;
        $('input[name="idCardImgTopFileUrl"]').val(fileUrl);
        $('input[name="idCardImgTopFileName"]').val(fileName);
        $('input[name="idCardImgTopFileSize"]').val(fileSize);

    });
    //清空预览图片的响应事件
    $("#fileinput-only-lay-certificate").on("filecleared",function(event, data, msg){
        $('input[name="idCardImgTopFileUrl"]').val("");
        $('input[name="idCardImgTopFileName"]').val("");
        $('input[name="idCardImgTopFileSize"]').val("");
    });

    //上传法人身份证反面照片信息
    $("#fileinput-only-company-legal-bottom").fileinput({
        //详细参数配置请参考  http://doc.ruoyi.vip/ruoyi/document/zjwd.html#jasny-bootstrap
        'theme': 'explorer-fas',
        'uploadUrl':  'oss/uploadFile',
        overwriteInitial: false,
        initialPreviewAsData: true,
        maxFileCount: 1, //表示允许同时上传的最大文件个数
        enctype: 'multipart/form-data',
        validateInitialCount:true,
        allowedFileExtensions:["jpg", "png", "jpeg", "pdf"],
        msgFilesTooMany: "选择上传的文件数量({n}) 超过允许的最大数值{m}！",
        autoReplace: false, //是否自动替换当前图
        uploadAsync: true, //是否为异步上传
        //默认显示的图片预览
        initialPreview: [
            //"/img/profile.jpg"
        ]
    });
    //上传成功之后的处理
    $("#fileinput-only-company-legal-bottom").on("fileuploaded", function(event, data, proviewId, index) {
        var fileUrl = data.response.fileUrl;
        var fileSize = data.response.fileSize;
        var fileName = data.response.fileName;
        $('input[name="legalPersonCardBottomFileUrl"]').val(fileUrl);
        $('input[name="legalPersonCardBottomFileName"]').val(fileName);
        $('input[name="legalPersonCardBottomFileSize"]').val(fileSize);

    });
    //清空预览图片的响应事件
    $("#fileinput-only-company-legal-bottom").on("filecleared",function(event, data, msg){
        $('input[name="legalPersonCardBottomFileUrl"]').val("");
        $('input[name="legalPersonCardBottomFileName"]').val("");
        $('input[name="legalPersonCardBottomFileSize"]').val("");
    });

    //上传营业执照副本照片信息
    $("#fileinput-only-company-business").fileinput({
        //详细参数配置请参考  http://doc.ruoyi.vip/ruoyi/document/zjwd.html#jasny-bootstrap
        'theme': 'explorer-fas',
        'uploadUrl': '/oss/uploadFile',
        overwriteInitial: false,
        initialPreviewAsData: true,
        maxFileCount: 1, //表示允许同时上传的最大文件个数
        enctype: 'multipart/form-data',
        validateInitialCount:true,
        allowedFileExtensions:["jpg", "png", "jpeg", "pdf"],
        msgFilesTooMany: "选择上传的文件数量({n}) 超过允许的最大数值{m}！",
        autoReplace: false, //是否自动替换当前图
        uploadAsync: true, //是否为异步上传
        //默认显示的图片预览
        initialPreview: [
            //"/img/profile.jpg"
        ]
    });
    //上传成功之后的处理
    $("#fileinput-only-company-business").on("fileuploaded", function(event, data, proviewId, index) {
        var fileUrl = data.response.fileUrl;
        var fileSize = data.response.fileSize;
        var fileName = data.response.fileName;
        $('input[name="idCardImgTopFileUrl"]').val(fileUrl);
        $('input[name="idCardImgTopFileName"]').val(fileName);
        $('input[name="idCardImgTopFileSize"]').val(fileSize);

    });
    //清空预览图片的响应事件
    $("#fileinput-only-company-business").on("filecleared",function(event, data, msg){
        $('input[name="idCardImgTopFileUrl"]').val("");
        $('input[name="idCardImgTopFileName"]').val("");
        $('input[name="idCardImgTopFileSize"]').val("");
    });
}

//=======================================律师注册相关=========================================
//==========================================================================================
/**
 * 律师注册页面数据初始化
 *
 * @param null
 * @author Qiu Rui
 * @date 2020/11/7 8:26 上午
 * @return
 */
function initLawData() {
    $("#lawyer_submit").on("click",function () {
        if ($('#lawyer_signupForm').validate({
            rules: {
                realName: {
                    required: true,
                    minlength: 2,
                    maxlength:10
                },
                idCard: {
                    required: true,
                    maxlength: 18,
                    minlength:6
                },
                contactName: {
                    required: true
                },
                job: {
                    required: true
                },
                phone: {
                    required: true,
                    isPhone: true,
                    isUniquePhone:true
                },
                phoneCode: {
                    required: true,
                    maxlength: 6
                },
                password: {
                    required: true,
                    minlength:6,
                    maxlength: 20
                },
                confirmPassword: {
                    required: true,
                    minlength:6,
                    maxlength: 20,
                    equalTo: "#lawyer_password"
                },
                businessImg:{
                    required: true
                },
                lawyerAcceptTerm: {
                    isRead:true
                }
            },
            messages: {
                realName: {
                    required: icon + "请输入您的姓名",
                    minlength: icon + "姓名必须在2个字以上",
                    maxlength: icon + "姓名必须在10个字以下"
                },
                idCard: {
                    required: icon + "请输入您的身份证号",
                    maxlength: icon + "身份证号必须在18位以下",
                    isIdCard: icon + "身份证号码格式不正确"
                },
                contactName: {
                    required: icon + "请输入您的就职律所"
                },
                job: {
                    required: icon + "请输入您的当前职务"
                },
                phone: {
                    required: icon + "请输入您的手机号码",
                    isPhone: icon + "手机号码格式不正确",
                    isUniquePhone: icon + "手机号已经被注册"
                },
                phoneCode: {
                    required: icon + "请输入您的手机验证码",
                    maxlength: icon + "手机验证码不能超过6位"
                },
                password: {
                    required: icon + "请输入您的密码",
                    minlength: icon + "密码必须6个字符以上",
                    maxlength: icon + "密码必须20个字符以下"
                },
                confirmPassword: {
                    required: icon + "请再次输入密码",
                    minlength: icon + "密码必须6个字符以上",
                    maxlength: icon + "密码必须20个字符以下",
                    equalTo: icon + "两次输入的密码不一致"
                },
                businessImg:{
                    required: icon + "请上传职业证书图片"
                },
                lawyerAcceptTerm: {
                    isRead:icon + "必须勾选同意注册协议"
                }
            }
        }).form()){
            $.ajax({
                url:"/registerAction",
                type: "post",
                dataType: "json",
                data: $('#lawyer_signupForm').serialize(),
                success: function (result) {
                    if (result.code == web_status.SUCCESS) {
                        layer.msg("注册成功", {
                            icon: 1,
                            time: 1500,
                            shade: [0.1, '#8F8F8F']
                        },function() {
                            window.location.href = "/login";
                        });
                    } else {
                        $.modal.alertWarning(result.msg);
                    }
                }
            });
            $.ajax(config)
        }
    });
    $("#fileinput-only-lawyer").fileinput({
        //详细参数配置请参考  http://doc.ruoyi.vip/ruoyi/document/zjwd.html#jasny-bootstrap
        'theme': 'explorer-fas',
        'uploadUrl': 'oss/uploadFile',
        overwriteInitial: false,
        initialPreviewAsData: true,
        maxFileCount: 1, //表示允许同时上传的最大文件个数
        enctype: 'multipart/form-data',
        validateInitialCount:true,
        allowedFileExtensions:["jpg", "png", "jpeg", "pdf"],
        msgFilesTooMany: "选择上传的文件数量({n}) 超过允许的最大数值{m}！",
        autoReplace: false, //是否自动替换当前图
        uploadAsync: true, //是否为异步上传
        //默认显示的图片预览
        initialPreview: [
            //"/img/profile.jpg"
        ]
    });
    //上传成功之后的处理
    $("#fileinput-only-lawyer").on("fileuploaded", function(event, data, proviewId, index) {
        var url = data.response.data.attachmentUrl;
        var name=data.response.data.attachmentName;

    });
    //清空预览图片的响应事件
    $("#fileinput-only-lawyer").on("filecleared",function(event, data, msg){

    });
}

//=======================================律所注册相关=========================================
//==========================================================================================
/**
 * 律所注册页面初始化
 *
 * @param null
 * @author Qiu Rui
 * @date 2020/11/7 8:45 上午
 * @return
 */
function initLawFirmData() {
    $("#lawFirm_submit").on("click",function () {
        if ($('#lawFirm_signupForm').validate({
            rules: {
                realName: {
                    required: true
                },
                idCard: {
                    required: true
                },
                address: {
                    required: true
                },
                contactName:{
                    required: true
                },
                phone: {
                    required: true,
                    isPhone: true,
                    isUniquePhone:true
                },
                phoneCode: {
                    required: true,
                    maxlength: 6
                },
                password: {
                    required: true,
                    minlength:6,
                    maxlength: 20
                },
                confirmPassword: {
                    required: true,
                    minlength:6,
                    maxlength: 20,
                    equalTo: "#lawFirm_password"
                },
                businessImg:{
                    required: true
                },
                lawFirmAcceptTerm: {
                    isRead:true
                }
            },
            messages: {
                realName: {
                    required: icon + "请输入律所名称"
                },
                idCard: {
                    required: icon + "请输入信用代码"
                },
                address: {
                    required: icon + "请输入律所地址"
                },
                contactName:{
                    required: icon + "请输入联系人"
                },
                phone: {
                    required: icon + "请输入您的手机号码",
                    isPhone: icon + "手机号码格式不正确",
                    isUniquePhone: icon + "手机号已经被注册"
                },
                phoneCode: {
                    required: icon + "请输入您的手机验证码",
                    maxlength: icon + "手机验证码不能超过6位"
                },
                password: {
                    required: icon + "请输入您的密码",
                    minlength: icon + "密码必须6个字符以上",
                    maxlength: icon + "密码必须20个字符以下"
                },
                confirmPassword: {
                    required: icon + "请再次输入密码",
                    minlength: icon + "密码必须6个字符以上",
                    maxlength: icon + "密码必须20个字符以下",
                    equalTo: icon + "两次输入的密码不一致"
                },
                businessImg:{
                    required: icon + "请上传职业许可证图片"
                },
                lawFirmAcceptTerm: {
                    isRead:icon + "必须勾选同意注册协议"
                }
            }
        }).form()){
            $.ajax({
                url:"/registerAction",
                type: "post",
                dataType: "json",
                data: $('#lawFirm_signupForm').serialize(),
                success: function (result) {
                    if (result.code == web_status.SUCCESS) {
                        layer.msg("注册成功", {
                            icon: 1,
                            time: 1500,
                            shade: [0.1, '#8F8F8F']
                        },function() {
                            window.location.href = "/login";
                        });
                    } else {
                        $.modal.alertWarning(result.msg);
                    }
                }
            });
            $.ajax(config)
        }
    });
    //上传法人身份证照片信息
    $("#fileinput-only-lawFirm").fileinput({
        //详细参数配置请参考  http://doc.ruoyi.vip/ruoyi/document/zjwd.html#jasny-bootstrap
        'theme': 'explorer-fas',
        'uploadUrl': '/oss/uploadFile',
        overwriteInitial: false,
        initialPreviewAsData: true,
        maxFileCount: 1, //表示允许同时上传的最大文件个数
        enctype: 'multipart/form-data',
        validateInitialCount:true,
        allowedFileExtensions:["jpg", "png", "jpeg", "pdf"],
        msgFilesTooMany: "选择上传的文件数量({n}) 超过允许的最大数值{m}！",
        autoReplace: false, //是否自动替换当前图
        uploadAsync: true, //是否为异步上传
        //默认显示的图片预览
        initialPreview: [
            //"/img/profile.jpg"
        ]
    });
    //上传成功之后的处理
    $("#fileinput-only-lawFirm").on("fileuploaded", function(event, data, proviewId, index) {
        var fileUrl = data.response.fileUrl;
        var fileSize = data.response.fileSize;
        var fileName = data.response.fileName;
        $('input[name="idCardImgTopFileUrl"]').val(fileUrl);
        $('input[name="idCardImgTopFileName"]').val(fileName);
        $('input[name="idCardImgTopFileSize"]').val(fileSize);
    });
    //清空预览图片的响应事件
    $("#fileinput-only-lawFirm").on("filecleared",function(event, data, msg){
        $('input[name="idCardImgTopFileUrl"]').val("");
        $('input[name="idCardImgTopFileName"]').val("");
        $('input[name="idCardImgTopFileSize"]').val("");
    });
}

//=======================================核检员注册相关=========================================
//==========================================================================================
/**
 * 核检员注册页面数据初始化
 *
 * @param null
 * @author Qiu Rui
 * @date 2020/11/7 8:58 上午
 * @return
 */
function initManagerData() {
    $("#manager_submit").on("click",function () {
        if ($('#manager_signupForm').validate({
            rules: {
                realName: {
                    required: true,
                    minlength: 2,
                    maxlength:10
                },
                phone: {
                    required: true,
                    isPhone: true,
                    isUniquePhone:true
                },
                phoneCode: {
                    required: true,
                    maxlength: 6
                },
                password: {
                    required: true,
                    minlength:6,
                    maxlength: 20
                },
                confirmPassword: {
                    required: true,
                    minlength:6,
                    maxlength: 20,
                    equalTo: "#manager_password"
                },
                managerAcceptTerm: {
                    isRead:true
                }
            },
            messages: {
                realName: {
                    required: icon + "请输入您的姓名",
                    minlength: icon + "姓名必须在2个字以上",
                    maxlength: icon + "姓名必须在10个字以下"
                },
                phone: {
                    required: icon + "请输入您的手机号码",
                    isPhone: icon + "手机号码格式不正确",
                    isUniquePhone: icon + "手机号已经被注册"
                },
                phoneCode: {
                    required: icon + "请输入您的手机验证码",
                    maxlength: icon + "手机验证码不能超过6位"
                },
                password: {
                    required: icon + "请输入您的密码",
                    minlength: icon + "密码必须6个字符以上",
                    maxlength: icon + "密码必须20个字符以下"
                },
                confirmPassword: {
                    required: icon + "请再次输入密码",
                    minlength: icon + "密码必须6个字符以上",
                    maxlength: icon + "密码必须20个字符以下",
                    equalTo: icon + "两次输入的密码不一致"
                },
                managerAcceptTerm: {
                    isRead:icon + "必须勾选同意注册协议"
                }
            }
        }).form()){
            $.ajax({
                url:"/registerAction",
                type: "post",
                dataType: "json",
                data: $('#manager_signupForm').serialize(),
                success: function (result) {
                    if (result.code == web_status.SUCCESS) {
                        layer.msg("注册成功", {
                            icon: 1,
                            time: 1500,
                            shade: [0.1, '#8F8F8F']
                        },function() {
                            window.location.href = "/login";
                        });
                    } else {
                        $.modal.alertWarning(result.msg);
                    }
                }
            });
            $.ajax(config)
        }
    });
}

/**
 * 发送验证码
 *
 * @param null
 * @author Qiu Rui
 * @date 2020/11/8 10:14 上午
 * @return
 */
function sendPhoneCode() {
    var phone = '';
    var personPhone = $('#personPhone').val();
    var companyPhone = $('#companyPhone').val();
    var lawyerPhone = $('#lawyerPhone').val();
    var lawFirmPhone = $('#lawFirmPhone').val();
    var adminPhone = $('#adminPhone').val();
    if (personPhone != '') {
        phone = personPhone;
    }
    if (companyPhone != '') {
        phone = companyPhone;
    }
    if (lawyerPhone != '') {
        phone = lawyerPhone;
    }
    if (lawFirmPhone != '') {
        phone = lawFirmPhone;
    }
    if (adminPhone != '') {
        phone = adminPhone;
    }
    $.operate.post( "/sendRegisterCaptcha" , { "phone": phone});
}

function initAddressData(){
    $.ajax({
        url: "getRegionByLevel/2",
        type: "get",
        success: function(result) {
            var typelist=$('#frist');
            typelist.empty();
            var second=$('#second');
            second.empty();
            var third=$('#third');
            third.empty();
            typelist.append('<option value="">' +"请选择"+ '</option>');
            second.append('<option value="">' +"请选择"+ '</option>');
            third.append('<option value="">' +"请选择"+ '</option>');
            $.each(result.data, function (index, item) {
                //向select 标签中添加数据
                typelist.append('<option value="' + item.id + '">' + item.regionName + '</option>');

            });
            var typelist1=$('#frist1');
            typelist1.empty();
            var second1=$('#second1');
            second1.empty();
            var third1=$('#third1');
            third1.empty();
            typelist1.append('<option value="">' +"请选择"+ '</option>');
            second1.append('<option value="">' +"请选择"+ '</option>');
            third1.append('<option value="">' +"请选择"+ '</option>');
            $.each(result.data, function (index, item) {
                //向select 标签中添加数据
                typelist1.append('<option value="' + item.id + '">' + item.regionName + '</option>');

            });

        },
        fail: function () {
            $.modal.alertWarning("系统异常，请联系系统管理员！");
        }
    });
}


