var prefix = ctx + "business/caseMark";
function saveMarkStepOne() {
    var markName = $("#markName").val();
    if(markName == null || markName ==''){
        $.modal.alertWarning("标名不能为空");
        return false;
    }
    $.ajax({
        url: prefix + "/saveMarkStepOne",
        data: $('#caseMarkStep-1').serialize(),
        type: "post",
        async: false,
        success: function (result) {
            if (result.code == 0) {
                $("#mark_id").val(result.data);
               return true;
            } else {
                $.modal.alertWarning(result.msg);
                return false;
            }
        },
        fail: function () {
            $.modal.alertWarning("系统异常，请联系系统管理员！");
            return false;
        }
    });
}
function markCaseCacheListPage() {
    var markId = $("#mark_id").val();
    if(markId == null || markId == ''){
       return false;
    }
    $.table.destroy();
    var options = {
        url: prefix + "/list/"+markId,
        modalName: "案件信息",
        columns: [{
            checkbox: true
        },
            {
                field: 'caseNo',
                title: '案件编号'
            },
            {
                field: 'applicantName',
                title: '申请人',
            },
            {
                field: 'proposerName',
                title: '被执行人',
            },
            {
                field: 'caseType',
                title: '案件类型',
                formatter: function(value, row, index) {
                    switch (value) {
                        case 1: return '房贷';
                        case 2: return '车贷';
                        case 3: return '消费贷';
                        case 4: return '信用贷';
                        case 5: return '其它';
                        default: return value;
                    }
                }
            },

            {
                field: 'execProvince',
                title: '执行管辖省',
            },
            {
                field: 'biddingAmount',
                title: '标的',
            },

            {
                field: 'commission',
                title: '佣金',
            },
            {
                title: '操作',
                align: 'center',
                formatter: function(value, row, index) {
                    return '<a class="btn btn-success btn-xs " href="javascript:void(0)" onclick="deleteMarkCase(' + row.caseId + ')"><i class="fa fa-info"></i>删除</a>';
                }
            }]
    };
    $.table.init(options);
    move();
}
function deleteMarkCase(caseId) {
    var markId = $("#mark_id").val();
    $.ajax({
        url: prefix + "/deleteMarkCase/"+markId,
        data: {"caseId":caseId},
        type: "post",
        success: function (result) {
            if (result.code == 0) {
                $.table.refresh();
                return true;
            } else {
                $.modal.alertWarning(result.msg);
                return false;
            }
        },
        fail: function () {
            $.modal.alertWarning("系统异常，请联系系统管理员！");
            return false;
        }
    });
}
function markOn() {
    var markId = $("#mark_id").val();

    if($('#bootstrap-table').length === 0){
        $.modal.alertWarning("没有需要上标的案件");
        return false;
    }
    $.ajax({
        url: prefix + "/markOn/"+markId ,
        type: "get",
        success: function (result) {
            if (result.code == 0) {
                $("#case_num").text(result.data);
                console.log(result.data);
                $("#mark_id").val(null);
                return true;
            } else {
                $.modal.alertWarning(result.msg);
                return false;
            }
        },
        fail: function () {
            $.modal.alertWarning("系统异常，请联系系统管理员！");
            return false;
        }
    });
}
