/*! Distpicker v2.0.7 | (c) 2014-present Chen Fengyuan | MIT */
(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(require('jquery')) :
  typeof define === 'function' && define.amd ? define(['jquery'], factory) :
  (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global.jQuery));
}(this, (function ($) { 'use strict';

  function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

  var $__default = /*#__PURE__*/_interopDefaultLegacy($);

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }

  var DEFAULTS = {
    // Selects the districts automatically.
    // 0 -> Disable autoselect
    // 1 -> Autoselect province only
    // 2 -> Autoselect province and city only
    // 3 -> Autoselect all (province, city and district)
    autoselect: 0,
    // Show placeholder.
    placeholder: true,
    // Select value. Options: 'name' and 'code'
    valueType: 'code',
    // Defines the initial value of province.
    province: '',
    // Defines the initial value of city.
    city: '',
    // Defines the initial value of district.
    district: ''
  };

  var DISTRICTS = {
    100000: {
      '': '数据加载失败'
    }
  };

  var WINDOW = typeof window !== 'undefined' ? window : {};
  var NAMESPACE = 'distpicker';
  var EVENT_CHANGE = 'change';

  var DEFAULT_CODE = 100000;
  var PROVINCE = 'province';
  var CITY = 'city';
  var DISTRICT = 'district';

  var Distpicker = /*#__PURE__*/function () {
    function Distpicker(element, options) {
      _classCallCheck(this, Distpicker);

      this.$element = $__default['default'](element);
      this.options = $__default['default'].extend({}, DEFAULTS, $__default['default'].isPlainObject(options) && options);
      this.placeholders = $__default['default'].extend({}, DEFAULTS);
      this.ready = false;
      this.init();
    }

    _createClass(Distpicker, [{
      key: "init",
      value: function init() {
        var _this = this;

        var options = this.options;
        var $selects = this.$element.find('select');
        var length = $selects.length;
        var data = {};
        $selects.each(function (i, select) {
          return $__default['default'].extend(data, $__default['default'](select).data());
        });
        $__default['default'].each([PROVINCE, CITY, DISTRICT], function (i, type) {
          if (data[type]) {
            options[type] = data[type];
            _this["$".concat(type)] = $selects.filter("[data-".concat(type, "]"));
          } else {
            _this["$".concat(type)] = length > i ? $selects.eq(i) : null;
          }
        });
        this.readData();
        this.bind(); // Reset all the selects (after event binding)
        this.reset();
        this.ready = true;
      }
    }, {
      key: "readData",
      value: function readData() {
        var cacheKey = "case-management:dist-picker:data";
        var cacheData = sessionStorage.getItem(cacheKey);
        if (cacheData != null) {
            DISTRICTS = JSON.parse(cacheData);
            return;
        }
        var dataUrl = ctx + "business/distpicker/data/" + DEFAULT_CODE;
        $.ajax({
          url: dataUrl,
          async: false,
          success: function (data) {
            if (data != null) {
              DISTRICTS = data;
              sessionStorage.setItem(cacheKey, JSON.stringify(data));
            } else {
              console.log("from url:" + dataUrl + " read data is null. used default data.|" + JSON.stringify(data));
            }
          },
          fail: function () {
            console.log("readData fail. used default data.");
          }
        });
      }
    }, {
      key: "bind",
      value: function bind() {
        var _this2 = this;

        if (this.$province) {
          this.$province.on(EVENT_CHANGE, this.onChangeProvince = $__default['default'].proxy(function () {
            _this2.output(CITY);

            _this2.output(DISTRICT, true);
          }, this));
        }

        if (this.$city) {
          this.$city.on(EVENT_CHANGE, this.onChangeCity = $__default['default'].proxy(function () {
            return _this2.output(DISTRICT, true);
          }, this));
        }
      }
    }, {
      key: "unbind",
      value: function unbind() {
        if (this.$province) {
          this.$province.off(EVENT_CHANGE, this.onChangeProvince);
        }

        if (this.$city) {
          this.$city.off(EVENT_CHANGE, this.onChangeCity);
        }
      }
    }, {
      key: "output",
      value: function output(type) {
        var triggerEvent = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
        var options = this.options,
            placeholders = this.placeholders;
        var $select = this["$".concat(type)];

        if (!$select || !$select.length) {
          return;
        }

        var code;

        switch (type) {
          case PROVINCE:
            code = DEFAULT_CODE;
            break;

          case CITY:
            code = this.$province && (this.$province.find(':selected').data('code') || '');
            break;

          case DISTRICT:
            code = this.$city && (this.$city.find(':selected').data('code') || '');
            break;
        }

        var districts = this.getDistricts(code);
        var value = options[type];
        var data = [];
        var matched = false;

        if ($__default['default'].isPlainObject(districts)) {
          $__default['default'].each(districts, function (i, name) {
            var selected = name === value || i === String(value);

            if (selected) {
              matched = true;
            }

            data.push({
              name: name,
              selected: selected,
              code: i,
              value: options.valueType === 'name' ? name : i
            });
          });
        }

        if (!matched) {
          var autoselect = options.autoselect || options.autoSelect;

          if (data.length && (type === PROVINCE && autoselect > 0 || type === CITY && autoselect > 1 || type === DISTRICT && autoselect > 2)) {
            data[0].selected = true;
          } // Save the unmatched value as a placeholder at the first output


          if (!this.ready && value) {
            placeholders[type] = value;
          }
        } // Add placeholder option


        if (options.placeholder) {
          data.unshift({
            code: '',
            name: placeholders[type],
            value: '',
            selected: false
          });
        }

        if (data.length) {
          $select.html(this.getList(data));
        } else {
          $select.empty();
        }

        if (triggerEvent) {
          $select.trigger(EVENT_CHANGE);
        }
      } // eslint-disable-next-line class-methods-use-this

    }, {
      key: "getList",
      value: function getList(data) {
        var list = [];
        $__default['default'].each(data, function (i, n) {
          var attrs = ["data-code=\"".concat(n.code, "\""), "data-text=\"".concat(n.name, "\""), "value=\"".concat(n.value, "\"")];

          if (n.selected) {
            attrs.push('selected');
          }

          list.push("<option ".concat(attrs.join(' '), ">").concat(n.name, "</option>"));
        });
        return list.join('');
      } // eslint-disable-next-line class-methods-use-this

    }, {
      key: "getDistricts",
      value: function getDistricts() {
        var code = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : DEFAULT_CODE;
        return DISTRICTS[code] || null;
      }
    }, {
      key: "reset",
      value: function reset(deep) {
        if (!deep) {
          this.output(PROVINCE);
          this.output(CITY);
          this.output(DISTRICT);
        } else if (this.$province) {
          this.$province.find(':first').prop('selected', true).end().trigger(EVENT_CHANGE);
        }
      }
    }, {
      key: "destroy",
      value: function destroy() {
        this.unbind();
      }
    }], [{
      key: "setDefaults",
      value: function setDefaults(options) {
        $__default['default'].extend(DEFAULTS, $__default['default'].isPlainObject(options) && options);
      }
    }]);

    return Distpicker;
  }();

  if ($__default['default'].fn) {
    var AnotherDistpicker = $__default['default'].fn.distpicker;

    $__default['default'].fn.distpicker = function jQueryDistpicker(option) {
      for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        args[_key - 1] = arguments[_key];
      }

      var result;
      this.each(function (i, element) {
        var $element = $__default['default'](element);
        var isDestroy = option === 'destroy';
        var distpicker = $element.data(NAMESPACE);

        if (!distpicker) {
          if (isDestroy) {
            return;
          }

          var options = $__default['default'].extend({}, $element.data(), $__default['default'].isPlainObject(option) && option);
          distpicker = new Distpicker(element, options);
          $element.data(NAMESPACE, distpicker);
        }

        if (typeof option === 'string') {
          var fn = distpicker[option];

          if ($__default['default'].isFunction(fn)) {
            result = fn.apply(distpicker, args);

            if (isDestroy) {
              $element.removeData(NAMESPACE);
            }
          }
        }
      });
      return typeof result === 'undefined' ? this : result;
    };

    $__default['default'].fn.distpicker.Constructor = Distpicker;
    $__default['default'].fn.distpicker.setDefaults = Distpicker.setDefaults;

    $__default['default'].fn.distpicker.noConflict = function noConflict() {
      $__default['default'].fn.distpicker = AnotherDistpicker;
      return this;
    };
  }

  if (WINDOW.document) {
    $__default['default'](function () {
      $__default['default']("[data-toggle=\"".concat(NAMESPACE, "\"]")).distpicker();
    });
  }

})));
