/**
 * 校验证据信息
 *
 * @param null
 * @author Qiu Rui
 * @date 2020/10/31 12:06 下午
 * @return
 */
function checkEvidenceParam() {
    if (!checkEvidenceInfoParam()){
        return false;
    }
    var rows=$("#bootstrap-table-evi").bootstrapTable("getData").length;
    if (rows<=0){
        $.modal.alertWarning('证据信息为空');
        return false;
    }
    return true;
}

/**
 * 校验被执行人信息
 *
 * @param null
 * @author Qiu Rui
 * @date 2020/11/10 11:09 下午
 * @return
 */
function checkProposerParam() {
    var boRows=$("#bootstrap-table-bo").bootstrapTable("getData").length;
    if (boRows<=0){
        $.modal.alertWarning('被被执行人信息为空');
        return false;
    }
    return true;
}

//手机号码验证
jQuery.validator.addMethod("isPhone", function(value, element) {
    var length = value.length;
    var mobile = /^(13[0-9]{9})|(18[0-9]{9})|(14[0-9]{9})|(17[0-9]{9})|(15[0-9]{9})$/;
    return this.optional(element) || (length == 11 && mobile.test(value));
}, "手机号码格式不正确");
/**************************************************************************
 身份号码排列顺序从左至右依次为：六位数字地址码，八位数字出生日期码，三位数字顺序码和一位数字校验码。
 地址码表示编码对象常住户口所在县(市、旗、区)的行政区划代码。
 出生日期码表示编码对象出生的年、月、日，其中年份用四位数字表示，年、月、日之间不用分隔符。
 顺序码表示同一地址码所标识的区域范围内，对同年、月、日出生的人员编定的顺序号。
 顺序码的奇数分给男性，偶数分给女性。
 校验码是根据前面十七位数字码，按照ISO 7064:1983.MOD 11-2校验码计算出来的检验码。
 15位校验规则 6位地址编码+6位出生日期+3位顺序号
 18位校验规则 6位地址编码+8位出生日期+3位顺序号+1位校验位
 校验位规则     公式:∑(ai×Wi)(mod 11)……………………………………(1)
 公式(1)中：
 i----表示号码字符从右至左包括校验码在内的位置序号；
 ai----表示第i位置上的号码字符值；
 Wi----示第i位置上的加权因子，其数值依据公式Wi=2^(n-1）(mod 11)计算得出。
 i 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1
 Wi 7 9 10 5 8 4 2 1 6 3 7 9 10 5 8 4 2 1
 ****************************************************************************/
//检查号码是否符合规范，包括长度，类型
function idenityCode_isCardNo(card) {
    //身份证号码为15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X
    var reg = /(^\d{15}$)|(^\d{17}(\d|X)$)/; // 正则表达式
    if (reg.test(card) === false) {
        return false;
    }
    return true;
};

//身份证格式验证
jQuery.validator.addMethod("isCard", function(value, element) {
    return idenityCode_isCardNo(value);
}, "身份证号格式不正确");

/**
 * 第一步参数校验
 *
 * @param null
 * @author Qiu Rui
 * @date 2020/11/11 2:33 下午
 * @return
 */
function checkCaseTypeParam() {
    return $("#caseTypeVo_form").validate({
        rules: {
            applicantEnterpriseIndustry: {
                required: true
            },
            caseType: {
                required: true
            },
            enforcementBasis: {
                required: true
            },
            loanGetWay: {
                required: true
            },
            execProvince: {
                required: true
            },
            execCity: {
                required: true
            }
        },
        messages: {
            applicantEnterpriseIndustry: {
                required: icon + "请选择公司性质"
            },
            caseType: {
                required: icon + "请选择案件类型"
            },
            enforcementBasis: {
                required: icon + "请选择执行依据"
            },
            loanGetWay: {
                required: icon + "请选择债权获得方式"
            },
            execProvince: {
                required: icon + "请选择执行管辖省"
            },
            execCity: {
                required: icon + "请选择执行选择市"
            }
        }
    }).form();
}

/**
 * 执行申请人参数校验
 *
 * @param null
 * @author Qiu Rui
 * @date 2020/11/11 2:34 下午
 * @return
 */
function checkApplicantUserInfoParam() {
    //执行申请人校验 applyImplementVo_naturalPerson_form  b_applyImplementVo_naturalPerson_form
    var partyType=$("#hidden_applyImplementVo_applicant_type").val();
    if (partyType==1){
        //自然人
        return $("#applyImplementVo_naturalPerson_form").validate({
            rules: {
                applicantName: {
                    required: true,
                    minlength: 2,
                    maxlength:10
                },
                applicantIdCardNo: {
                    required: true,
                    isCard: true
                },
                applicantMobile: {
                    required: true,
                    isPhone: true
                },
                applicantCountry: {
                    required: true
                },
                applicantNation: {
                    required: true
                },
                applicantAddress: {
                    required: true,
                    maxlength: 50
                }
            },
            messages: {
                applicantName: {
                    required: icon + "请输入姓名",
                    minlength: icon + "姓名必须在2个字以上",
                    maxlength: icon + "姓名必须在10个字以下"
                },
                applicantIdCardNo: {
                    required: icon + "请输入证件号",
                    isCard: icon+"身份证号格式不正确"
                },
                applicantMobile: {
                    required: icon + "请输入您的手机号码",
                    isPhone: icon + "手机号码格式不正确"
                },
                applicantCountry: {
                    required: icon + "请选择国籍"
                },
                applicantNation: {
                    required: icon + "请选择民族"
                },
                applicantAddress: {
                    required: icon + "请输入地址",
                    maxlength: icon + "通讯地址在50个字符以内"
                }
            }
        }).form();
    }else if (partyType==2){
        //法人
        return $("#b_applyImplementVo_legalPerson_form").validate({
            rules: {
                applicantName: {
                    required: true,
                    minlength: 2,
                    maxlength:20
                },
                applicantIdCardNo: {
                    required: true
                },
                applicantMobile: {
                    required: true,
                    isPhone: true
                },
                /*applicantCountry: {
                    required: true
                },
                applicantNation: {
                    required: true
                },*/
                applicantAddress: {
                    required: true,
                    maxlength: 50
                },
                applicantLegalPersonName: {
                    required: true,
                    minlength: 2,
                    maxlength:10
                }/*,
                applicantLegalPersonCard: {
                    required: true
                }*/
            },
            messages: {
                applicantName: {
                    required: icon + "请输入公司名称",
                    minlength: icon + "公司名称必须在2个字以上",
                    maxlength: icon + "公司名称必须在20个字以下"
                },
                applicantIdCardNo: {
                    required: icon + "请输入组织就够代码"
                },
                applicantMobile: {
                    required: icon + "请输入您的手机号码",
                    isPhone: icon + "手机号码格式不正确"
                },
               /* applicantCountry: {
                    required: icon + "请选择国籍"
                },
                applicantNation: {
                    required: icon + "请选择民族"
                },*/
                applicantAddress: {
                    required: icon + "请输入地址",
                    maxlength: icon + "通讯地址在50个字符以内"
                },
                applicantLegalPersonName: {
                    required: icon + "请输入法定代表人姓名",
                    minlength: icon + "法定代表人姓名必须在2个字以上",
                    maxlength: icon + "法定代表人姓名必须在10个字以下"
                }/*,
                applicantLegalPersonCard: {
                    required: icon + "请输入法定代表人证件号"
                }*/
            }
        }).form();
    }else {
        $.modal.alertWarning('类型错误');
        return false;
    }
    return true;
}

/**
 * 被执行人信息校验
 *
 * @param null
 * @author Qiu Rui
 * @date 2020/11/11 3:11 下午
 * @return
 */
function checkProposerUserInfoParam() {
    //proposer_naturalPerson_form  proposer_legalPerson_form
    var partyType=$("input[name='proposer_type']:checked").val();
    if (partyType==1){
        //自然人
        return $("#proposer_naturalPerson_form").validate({
            rules: {
                proposerName: {
                    required: true,
                    minlength: 2,
                    maxlength:10
                },
                proposerIdCardNo: {
                    required: true,
                    isCard: true
                },
                proposerMobile: {
                    required: true,
                    isPhone: true
                },
                proposerCountry: {
                    required: true
                },
                proposerNation: {
                    required: true
                },
                proposerAddress: {
                    required: true,
                    maxlength: 50
                }
            },
            messages: {
                proposerName: {
                    required: icon + "请输入姓名",
                    minlength: icon + "姓名必须在2个字以上",
                    maxlength: icon + "姓名必须在10个字以下"
                },
                proposerIdCardNo: {
                    required: icon + "请输入证件号",
                    isCard: icon+"身份证号格式不正确"
                },
                proposerMobile: {
                    required: icon + "请输入您的手机号码",
                    isPhone: icon + "手机号码格式不正确"
                },
                proposerCountry: {
                    required: icon + "请选择国籍"
                },
                proposerNation: {
                    required: icon + "请选择民族"
                },
                proposerAddress: {
                    required: icon + "请输入地址",
                    maxlength: icon + "通讯地址在50个字符以内"
                }
            }
        }).form();
    }else if (partyType==2){
        //法人
        return $("#proposer_legalPerson_form").validate({
            rules: {
                proposerName: {
                    required: true,
                    minlength: 2,
                    maxlength:20
                },
                proposerIdCardNo: {
                    required: true
                },
                proposerMobile: {
                    required: true,
                    isPhone: true
                },
                proposerCountry: {
                    required: true
                },
                proposerNation: {
                    required: true
                },
                proposerAddress: {
                    required: true,
                    maxlength: 50
                },
                proposerLegalPersonName: {
                    required: true,
                    minlength: 2,
                    maxlength:10
                },
                proposerLegalPersonCard: {
                    required: true,
                    isCard: true
                },
                proposerJob:{
                    required: true
                }
            },
            messages: {
                proposerName: {
                    required: icon + "请输入公司名称",
                    minlength: icon + "公司名称必须在2个字以上",
                    maxlength: icon + "公司名称必须在20个字以下"
                },
                proposerIdCardNo: {
                    required: icon + "请输入组织机构代码"
                },
                proposerMobile: {
                    required: icon + "请输入您的手机号码",
                    isPhone: icon + "手机号码格式不正确"
                },
                proposerCountry: {
                    required: icon + "请选择国籍"
                },
                proposerNation: {
                    required: icon + "请选择民族"
                },
                proposerAddress: {
                    required: icon + "请输入地址",
                    maxlength: icon + "通讯地址在50个字符以内"
                },
                proposerLegalPersonName: {
                    required: icon + "请输入法定代表人姓名",
                    minlength: icon + "法定代表人姓名必须在2个字以上",
                    maxlength: icon + "法定代表人姓名必须在10个字以下"
                },
                proposerLegalPersonCard: {
                    required: icon + "请输入法定代表人证件号",
                    isCard: icon+"法人证件号号格式不正确"
                }
                ,
                proposerJob: {
                    required: icon + "请输入法定代表人职务"
                }
            }
        }).form();
    }else {
        $.modal.alertWarning('类型错误');
        return false;
    }
}

/**
 * 校验证据信息
 *
 * @param null
 * @author Qiu Rui
 * @date 2020/11/11 3:29 下午
 * @return
 */
function checkEvidenceInfoParam() {
    return $("#evidenceVo_form").validate({
        rules: {
            biddingAmount: {
                required: true
            },
            arbitrationCommissionName: {
                required: true
            },
            overdueDay: {
                required: true
            },
            arbitramentNumber: {
                required: true
            }
        },
        messages: {
            biddingAmount: {
                required: icon + "请输入执行标的额"
            },
            arbitrationCommissionName: {
                required: icon + "请输入仲裁委全称"
            },
            overdueDay: {
                required: icon + "请输入逾期天数"
            },
            arbitramentNumber: {
                required: icon + "请输入逾期天数"
            }
        }
    }).form();
}