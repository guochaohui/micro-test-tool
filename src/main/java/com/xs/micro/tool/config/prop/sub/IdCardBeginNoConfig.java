package com.xs.micro.tool.config.prop.sub;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IdCardBeginNoConfig {
    private String code;
    private String name;
}
