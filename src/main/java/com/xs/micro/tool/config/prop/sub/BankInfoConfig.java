package com.xs.micro.tool.config.prop.sub;

public class BankInfoConfig {

    private String enName;
    private String cnName;
    private String beginNo;
    private Integer noLength;

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    public String getCnName() {
        return cnName;
    }

    public void setCnName(String cnName) {
        this.cnName = cnName;
    }

    public String getBeginNo() {
        return beginNo;
    }

    public void setBeginNo(String beginNo) {
        this.beginNo = beginNo;
    }

    public Integer getNoLength() {
        return noLength;
    }

    public void setNoLength(Integer noLength) {
        this.noLength = noLength;
    }
}
