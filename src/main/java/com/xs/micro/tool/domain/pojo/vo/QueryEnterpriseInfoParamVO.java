package com.xs.micro.tool.domain.pojo.vo;

import lombok.Data;

@Data
public class QueryEnterpriseInfoParamVO {

    private String keyword;
    private Integer maxCount;
    private Integer queryType;

}
