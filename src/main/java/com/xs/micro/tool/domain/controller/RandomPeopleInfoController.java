package com.xs.micro.tool.domain.controller;

import com.xs.micro.tool.config.prop.sub.IdCardBeginNoConfig;
import com.xs.micro.tool.domain.business.RandomPeopleInfoBusiness;
import com.xs.micro.tool.domain.pojo.vo.GenerateRandomPeopleInfoParamVO;
import com.xs.micro.tool.domain.pojo.vo.GenerateRandomPeopleInfoVO;
import com.xs.micro.tool.domain.pojo.vo.GlobalResponseEntity;
import com.xs.micro.tool.domain.pojo.vo.TableDataInfoVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * 生成随机个人信息
 *
 * @author guochaohui
 */
@ApiIgnore
@Controller
@RequestMapping(value = "/randomPeopleInfo")
public class RandomPeopleInfoController extends BaseController {

    @Autowired
    private RandomPeopleInfoBusiness randomPeopleInfoBusiness;

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public String page(Model model) {
        model.addAttribute("params", new GenerateRandomPeopleInfoParamVO());
        model.addAttribute("bankCardTypeList", randomPeopleInfoBusiness.getBankInfoList());
        model.addAttribute("provinceList", randomPeopleInfoBusiness.getProvinceList());
        return "randomPeopleInfo";

    }

    @RequestMapping(value = "/cityList/{provinceCode}", method = RequestMethod.POST)
    @ResponseBody
    public GlobalResponseEntity<List<IdCardBeginNoConfig>> cityList(@PathVariable("provinceCode") String provinceCode) {
        return GlobalResponseEntity.success(randomPeopleInfoBusiness.getCityList(provinceCode));
    }

    @RequestMapping(value = "/countyList/{cityCode}", method = RequestMethod.POST)
    @ResponseBody
    public GlobalResponseEntity<List<IdCardBeginNoConfig>> countyList(@PathVariable("cityCode") String cityCode) {
        return GlobalResponseEntity.success(randomPeopleInfoBusiness.getCountyList(cityCode));
    }

    @RequestMapping(value = "/generateRandomPeopleInfo", method = RequestMethod.POST)
    @ResponseBody
    public TableDataInfoVO generateRandomPeopleInfo(GenerateRandomPeopleInfoParamVO param) {
        List<GenerateRandomPeopleInfoVO> list = randomPeopleInfoBusiness.generateRandomPeopleInfo(param);
        TableDataInfoVO rspData = new TableDataInfoVO();
        rspData.setCode(0);
        rspData.setRows(list);
        rspData.setTotal(list.size());
        return rspData;
    }

}
