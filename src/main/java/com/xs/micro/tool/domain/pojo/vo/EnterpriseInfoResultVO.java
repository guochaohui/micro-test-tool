package com.xs.micro.tool.domain.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EnterpriseInfoResultVO {

    private String enterpriseName;
    private String enterpriseCreditCode;
    private String enterpriseRegisterNo;

}
