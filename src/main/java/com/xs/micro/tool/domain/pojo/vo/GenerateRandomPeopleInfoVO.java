package com.xs.micro.tool.domain.pojo.vo;

import lombok.Data;

@Data
public class GenerateRandomPeopleInfoVO {

    private String peopleName;
    private String peopleGender;
    private Integer peopleAge;
    private String peopleBirthday;
    private String peopleMobile;
    private String peopleIdCardNo;
    private BankCardInfoVO peopleBankCard;
    private IdCardAreaInfoVO idCardAreaInfo;

}
