package com.xs.micro.tool.domain.pojo.vo;

import lombok.Data;

import java.util.Date;

@Data
public class IdCardInfoVO {

    private String idCardNo;
    private Date birthday;
    private Integer age;
    private Integer gender;

    private IdCardAreaInfoVO areaInfo;

}
