package com.xs.micro.tool.domain.pojo.contants;

public class RandomPeopleInfoDataContants {

    public static final String SPLIT_DATA_INFO_STR = "#";
    public static final String AREA_CODE_PAD_STR = "0";
    public static final int AREA_CODE_LENGTH = 6;
    public static final int PROVINCE_CODE_PREFIX_LENGTH = 2;
    public static final int CITY_CODE_PREFIX_LENGTH = 4;

}
