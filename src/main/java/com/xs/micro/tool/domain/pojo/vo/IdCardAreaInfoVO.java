package com.xs.micro.tool.domain.pojo.vo;

import com.xs.micro.tool.config.prop.sub.IdCardBeginNoConfig;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IdCardAreaInfoVO {

    private IdCardBeginNoConfig province;
    private IdCardBeginNoConfig city;
    private IdCardBeginNoConfig county;

}
