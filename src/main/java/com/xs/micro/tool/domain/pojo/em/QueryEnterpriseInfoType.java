package com.xs.micro.tool.domain.pojo.em;

public enum QueryEnterpriseInfoType {
    /***/

    CREDIT_SHANG_HAI(4, "信用上海"),
    CREDIT_CHINA(1, "信用中国"),
    CREDIT_SHAN_DONG(2, "信用山东"),
    CREDIT_BEI_JING(3, "信用北京"),
    ;

    private QueryEnterpriseInfoType(int value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    private String desc;
    private int value;

    public static final QueryEnterpriseInfoType getByValue(int value) {
        for (QueryEnterpriseInfoType type : QueryEnterpriseInfoType.values()) {
            if (type.value == value) {
                return type;
            }
        }
        return null;
    }

    public String getDesc() {
        return desc;
    }

    public int getValue() {
        return value;
    }
}
