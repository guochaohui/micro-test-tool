package com.xs.micro.tool.domain.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 主页显示
 *
 * @author guochaohui
 */
@ApiIgnore
@Controller
public class IndexController extends BaseController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String indexPage(Model model) {
        return "index";
    }

    @RequestMapping(value = "/main", method = RequestMethod.GET)
    public String mainPage(Model model) {
        return "main";
    }

}
