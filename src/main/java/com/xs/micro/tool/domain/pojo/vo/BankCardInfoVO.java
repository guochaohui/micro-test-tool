package com.xs.micro.tool.domain.pojo.vo;

import lombok.Data;

@Data
public class BankCardInfoVO {

    private String cnName;
    private String enName;
    private String cardNo;

}
