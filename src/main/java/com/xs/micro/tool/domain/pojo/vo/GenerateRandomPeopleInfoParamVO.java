package com.xs.micro.tool.domain.pojo.vo;

import lombok.Data;

@Data
public class GenerateRandomPeopleInfoParamVO {

    private Integer generateCount;
    private Integer gender;
    private Integer bankCardNoCountOfEachPeople;
    private Boolean mobileOfEachPeopleIsTheSame;
    private Integer bankCardType;
    private String idCardNoProvinceCode;
    private String idCardNoCityCode;
    private String idCardNoCountyCode;

    public GenerateRandomPeopleInfoParamVO() {
        this.generateCount = 20;
        this.gender = -1;
        this.bankCardNoCountOfEachPeople = 1;
        this.mobileOfEachPeopleIsTheSame = true;
        this.bankCardType = -1;
        this.idCardNoProvinceCode = null;
        this.idCardNoCityCode = null;
        this.idCardNoCountyCode = null;
    }
}
