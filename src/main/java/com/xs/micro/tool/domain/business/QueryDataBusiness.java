package com.xs.micro.tool.domain.business;

import com.google.common.collect.Maps;
import com.xs.micro.tool.domain.pojo.em.QueryEnterpriseInfoType;
import com.xs.micro.tool.domain.pojo.vo.QueryEnterpriseInfoParamVO;
import com.xs.micro.tool.domain.pojo.vo.EnterpriseInfoResultVO;
import com.xs.micro.tool.extension.util.SpringUtil;
import com.xs.micro.tool.service.BaseQueryDataService;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;

/**
 * 查询数据业务
 *
 * @author guochaohui
 */
@Component
public class QueryDataBusiness {

    private Map<QueryEnterpriseInfoType, BaseQueryDataService> queryDataServiceMap;

    @PostConstruct
    public void initialDataServiceMap() {
        if (queryDataServiceMap != null) {
            return;
        }
        queryDataServiceMap = Maps.newHashMap();
        Map<String, BaseQueryDataService> beans = SpringUtil.getBeans(BaseQueryDataService.class);
        for (BaseQueryDataService service : beans.values()) {
            queryDataServiceMap.put(service.type(), service);
        }
    }

    /**
     * 查询企业信息
     *
     * @param queryParam
     * @return
     */
    public List<EnterpriseInfoResultVO> queryCreditDatas(QueryEnterpriseInfoParamVO queryParam) throws Exception {
        QueryEnterpriseInfoType type = QueryEnterpriseInfoType.getByValue(queryParam.getQueryType());
        Assert.notNull(type, "不支持的类型." + queryParam.getQueryType());

        BaseQueryDataService service = queryDataServiceMap.get(type);
        Assert.notNull(service, type.getDesc() + "-查询未实现");

        return service.query(queryParam);
    }

}
