package com.xs.micro.tool.domain.controller;

import com.xs.micro.tool.domain.business.QueryDataBusiness;
import com.xs.micro.tool.domain.pojo.em.QueryEnterpriseInfoType;
import com.xs.micro.tool.domain.pojo.vo.HttpStatusCode;
import com.xs.micro.tool.domain.pojo.vo.QueryEnterpriseInfoParamVO;
import com.xs.micro.tool.domain.pojo.vo.EnterpriseInfoResultVO;
import com.xs.micro.tool.domain.pojo.vo.TableDataInfoVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * 查询数据地址
 *
 * @author guochaohui
 */
@ApiIgnore
@Controller
@RequestMapping(value = "/queryData")
public class QueryDataController extends BaseController {

    @Autowired
    private QueryDataBusiness queryDataBusiness;

    @RequestMapping(value = "/queryEnterprisePage", method = RequestMethod.GET)
    public String queryEnterprisePage(Model model) {
        model.addAttribute("queryTypes", QueryEnterpriseInfoType.values());
        return "queryEnterpriseInfo";
    }

    @RequestMapping(value = "/queryEnterpriseInfo", method = RequestMethod.POST)
    @ResponseBody
    public TableDataInfoVO queryEnterpriseInfo(QueryEnterpriseInfoParamVO queryParam) throws Exception {
        TableDataInfoVO vo = new TableDataInfoVO();
        if (StringUtils.isBlank(queryParam.getKeyword())) {
            vo.setCode(HttpStatusCode.PARAMS_ERROR);
            vo.setMsg("企业关键字不能为空");
        } else {
            List<EnterpriseInfoResultVO> list = queryDataBusiness.queryCreditDatas(queryParam);
            vo.setCode(0);
            vo.setRows(list);
            vo.setTotal(list.size());
        }
        return vo;
    }

}
