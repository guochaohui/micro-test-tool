package com.xs.micro.tool;

import com.xs.micro.tool.domain.business.RandomPeopleInfoBusiness;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Random;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = {MicroToolApplication.class})
public class TestTool {

    @Autowired
    private RandomPeopleInfoBusiness randomPeopleInfoBusiness;

    @Test
    public void test() {
        System.out.println("******************************");
        for (int i = 0; i < 10; i++) {
            int gender = new Random().nextInt(2);
            System.out.println(randomPeopleInfoBusiness.generateRandomMobile());
            System.out.println(randomPeopleInfoBusiness.generateRandomPeopleName(gender));
            System.out.println(randomPeopleInfoBusiness.generateRandomIdCardInfo(null, null, null, gender, null));
            System.out.println("******************************");
        }
    }

}
